# D&D Generator Project

>## Repos:
> | Products |    |
> |:---------|---:|
> |CMS|[![pipeline status](https://gitlab.com/darkhounds/wotc/dnd-generator/product-cms/badges/master/pipeline.svg)](https://gitlab.com/darkhounds/wotc/dnd-generator/product-cms/commits/master)|
> |Client| |
>
><br/>
>
> | Services |    |
> |:---------|---:|
> |Authorization Producer|[![pipeline status](https://gitlab.com/darkhounds/wotc/dnd-generator/service-authorization-producer/badges/master/pipeline.svg)](https://gitlab.com/darkhounds/wotc/dnd-generator/service-authorization-producer/commits/master)|
> |Authorization Consumer|[![pipeline status](https://gitlab.com/darkhounds/wotc/dnd-generator/service-authorization-consumer/badges/master/pipeline.svg)](https://gitlab.com/darkhounds/wotc/dnd-generator/service-authorization-consumer/commits/master)|
> |Queue|[![pipeline status](https://gitlab.com/darkhounds/wotc/dnd-generator/service-queue/badges/master/pipeline.svg)](https://gitlab.com/darkhounds/wotc/dnd-generator/service-queue/commits/master)|
> |Hashtable|[![pipeline status](https://gitlab.com/darkhounds/wotc/dnd-generator/service-hashtable/badges/master/pipeline.svg)](https://gitlab.com/darkhounds/wotc/dnd-generator/service-hashtable/commits/master)|
> |Documents||
> |Data||

>## Stories:
>1. CMS website Authentication (dnd-generator%1):
project#1, project#2, project#3, project#4
>2. CMS website Characters section (dnd-generator%2)
project#5, project#6, project#7, project#8, project#9, project#14
>3. CMS website Places section (dnd-generator%3)
project#15, project#16, project#17, project#21
>4. Client website Characters Section (dnd-generator%4)
project#22, project#23, project#24, project#25, project#26
>5. Client website Places Section (dnd-generator%5)
project#27, project#28, project#29, project#30, project#31
